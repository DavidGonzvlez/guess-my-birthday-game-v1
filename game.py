from random import randint

# #### --- long verion --- ###



# #beigns by asking for our input, our name
# name = input("Hello, What is your name?")


# # the computer's first guess"
# guess_month = randint(1, 12)
# guess_year = randint(1924, 2004)

# print("Guess 1 :", name, "were you born in", guess_month, "/", guess_year, "?")

# response = input("yes or no?")

# if response == "yes":
#     print("I knew it")
#     exit()
# else:
#     print("dang it, lemme try again")

# # the computer's second guess"
# guess_month = randint(1, 12)
# guess_year = randint(1924, 2004)

# print("Guess 2 :", name, "were you born in", guess_month, "/", guess_year, "?")

# response = input("yes or no?")

# if response == "yes":
#     print("I knew it")
#     exit()
# else:
#     print("dang it, lemme try again")

# # the computer's third guess"
# guess_month = randint(1, 12)
# guess_year = randint(1924, 2004)

# print("Guess 3 :", name, "were you born in", guess_month, "/", guess_year, "?")

# response = input("yes or no?")

# if response == "yes":
#     print("I knew it")
#     exit()
# else:
#     print("dang it, lemme try again")

# # the computer's fourth guess"
# guess_month = randint(1, 12)
# guess_year = randint(1924, 2004)

# print("Guess 4 :", name, "were you born in", guess_month, "/", guess_year, "?")

# response = input("yes or no?")

# if response == "yes":
#     print("I knew it")
#     exit()
# else:
#     print("dang it, lemme try again")

# # the computer's fifth guess"
# guess_month = randint(1, 12)
# guess_year = randint(1924, 2004)

# print("Guess 5 :", name, "were you born in", guess_month, "/", guess_year, "?")

# response = input("yes or no?")

# if response == "yes":
#     print("I knew it")
#     exit()
# else:
#     print("")
#     exit()


# ### --- short verison --- ###


# #beigns by asking for our input, our name
name = input("Hello, What is your name?")


# the computer's first guess
# also including the for loop

for guess in range(1,6):
    guess_month = randint(1, 12)
    guess_year = randint(1924, 2004)

    print("Guess", guess, ":", name, "were you born in", guess_month, "/", guess_year, "?")

    response = input("yes or no?")

    if response == "yes":
        print("I knew it")
        exit()
    elif guess == 5:
        print("dang it, im bad at this, goodbye!")
    else:
        print("dang it, lemme try again")
